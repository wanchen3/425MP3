 /*
	SDFS server
*/

#include "share.h"

#define BACKLOG 10 // how many pending connections queue will hold
#define PATH_LENGTH 600 // the length of requested file path (GET & DELETE)

void make_socket(int & socket_file_descriptor, const char * port);
int find_max(int socket_put, int socket_get, int socket_delete, int socket_broadcast, int socket_join, int socket_replicate);
void load(int & socket_file_descriptor);
void pull(int & socket_file_descriptor);
void remove(int & socket_file_descriptor);
void replace_file_list(int & socket_file_descriptor, bool & filelist_initialized);
void onboard(int & socket_file_descriptor);
void request_file_list(bool & filelist_initialized);
void get_alive_IP(vector<string> & alive_IP);
void update_membrship_list(map< string, vector<string> > & file_list);
void check_failure();
string get_highest_IP(vector<string> & alive_IP);
void get_lost_file(set<string> & lost_file, vector<string> & alive_IP, map< string, vector<string> > & file_list);
void replicate(set<string> & lost_file, map< string, vector<string> > & file_list, string selfIP);
void replicate(set<string> & lost_file, vector<string> & local_file);
void replicate_handler(int & socket_file_descriptor);
string get_next_replicator(set<string> & lost_file);

int main(void)
{
	/* construct sockets */
	int socket_put;
	int socket_get;
	int socket_delete;
	int socket_broadcast;
	int socket_join;
	int socket_replicate;
	make_socket(socket_put, PUT_PORT);
	make_socket(socket_get, GET_PORT);
	make_socket(socket_delete, DELETE_PORT);
	make_socket(socket_broadcast, BROADCAST_PORT);
	make_socket(socket_join, JOIN_PORT);
	make_socket(socket_replicate, REPLICATE_PORT);

	fd_set readfds; // socket set for PUT and DELETE operations

	struct timeval timer; // select() timeout
	bool filelist_request_sent = 0;
	bool filelist_initialized = 0;
	while(1)
	{
		/* add all socket file descriptors to their corresponding socket sets */
		FD_ZERO(&readfds);
		FD_SET(socket_put, &readfds);
		FD_SET(socket_get, &readfds);
		FD_SET(socket_delete, &readfds);
		FD_SET(socket_broadcast, &readfds);
		FD_SET(socket_join, &readfds);
		FD_SET(socket_replicate, &readfds);

		/* set timeout period */
		timer.tv_sec = 3;
		timer.tv_usec = 0;

		/* select() */
		int numfds = find_max(socket_put, socket_get, socket_delete, socket_broadcast, socket_join, socket_replicate)+1;
		int num_ready = select(numfds, &readfds, NULL, NULL, &timer);
		if(num_ready == -1)
		{
			perror("select");
			exit(1);
		}
		if(num_ready)
		{
			if(FD_ISSET(socket_put, &readfds))
			{
				load(socket_put); // load file into SDFS
			}
			if(FD_ISSET(socket_get, &readfds))
			{
				pull(socket_get); // pull file from SDFS and send it to client
			}
			if(FD_ISSET(socket_delete, &readfds))
			{
				remove(socket_delete); // delete file in SDFS
			}
			if(FD_ISSET(socket_broadcast, &readfds))
			{
				replace_file_list(socket_broadcast, filelist_initialized); // replace local file list
			}
			if(FD_ISSET(socket_join, &readfds))
			{
				onboard(socket_join); // new VM joins, update and boradcase local file list
			}
			if(FD_ISSET(socket_replicate, &readfds))
			{
				replicate_handler(socket_replicate);
			}
		}
		if(!filelist_request_sent)
		{
			request_file_list(filelist_initialized);
			filelist_request_sent = 1;
		}
		else
		{
			check_failure(); // handle failure
		}
	}

	return 0;
}

/**********************************************************************/
/*********************** helper functions *******************************/
/**********************************************************************/

void make_socket(int & socket_file_descriptor, const char * port)
{
	struct addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	int result;
	struct addrinfo * server_information;
	if((result = getaddrinfo(NULL, port, &hints, &server_information)) != 0)
	{
		fprintf(stderr, "getaddrinfo(server make_socket()): %s\n", gai_strerror(result));
		exit(1);
	}

	/*** loop through all the results and bind to the first we can ***/
	struct addrinfo * temp;
	int yes = 1;
	for(temp = server_information; temp != NULL; temp = temp->ai_next)
	{
		if((socket_file_descriptor = socket(temp->ai_family, temp->ai_socktype, temp->ai_protocol)) == -1)
		{
			perror("server: socket");
			continue;
		}
		if(setsockopt(socket_file_descriptor, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1)
		{
			perror("setsockopt");
			exit(1);
		}
		if(bind(socket_file_descriptor, temp->ai_addr, temp->ai_addrlen) == -1)
		{
			if(close(socket_file_descriptor) == -1)
			{
				perror("close");
				exit(1);
			}
			perror("server: bind");
			continue;
		}

		break;
	}

	freeaddrinfo(server_information); // all done with this structure

	if(temp == NULL)
	{
		fprintf(stderr, "server: failed to bind\n");
		exit(1);
	}

	// tell a socket to listen for incoming connections
	if(listen(socket_file_descriptor, BACKLOG) == -1)
	{
		perror("listen");
		exit(1);
	}
}

int find_max(int socket_put, int socket_get, int socket_delete, int socket_broadcast, int socket_join, int socket_replicate)
{
	int retval = socket_put;
	(retval < socket_get) && (retval = socket_get);
	(retval < socket_delete) && (retval = socket_delete);
	(retval < socket_broadcast) && (retval = socket_broadcast);
	(retval < socket_join) && (retval = socket_join);
	(retval < socket_replicate) && (retval = socket_replicate);
	return retval;
}

void load(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		FILE * file_load;

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			if(!flag)
			{
				char * separator = strstr(buffer, ":");
				*separator = '\0';
				if((file_load = fopen(("./sdfs_file/"+string(buffer)).c_str(), "wb")) == NULL)
				{
					perror("fopen");
					exit(1);
				}
				separator = separator + sizeof(char);
				fwrite(separator, sizeof(char), num_recv-strlen(buffer)-1, file_load);
				
				flag = !flag;
			}
			else
			{
				fwrite(buffer, sizeof(char), num_recv, file_load);
			}
			memset(&buffer, 0, sizeof buffer);
		}

		if(fclose(file_load) == EOF)
		{
			perror("fclose");
			exit(1);
		}

		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

void pull(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		/*** get filename to be pulled from SDFS ***/
		char path[PATH_LENGTH];
		memset(&path, 0, sizeof path);
		ssize_t num_recv;
		if((num_recv = recv(new_connection, path, PATH_LENGTH, 0)) == -1)
		{
			perror("recv");
			exit(2);
		}
		path[num_recv] = '\0';

		//cout<<"start sending!"<<endl;

		/* send the file to client */
		FILE * file_get;
		if(( file_get = fopen(("./sdfs_file/"+ string(path)).c_str(), "rb") ) == NULL)
	        {
		  //cout<<"File does not exist!"<<endl;
		} 
		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		int num_read;
		int sent;
		while( (num_read = fread(buffer, sizeof(char), BUFFER_LENGTH, file_get)) > 0)
		  {
		    if(num_read < BUFFER_LENGTH)
		      {
			sent = send(new_connection, buffer, num_read, 0);
			string content = buffer;
			//cout<<"content:"<<buffer<<endl;
			if(sent == -1)
			  {
			    perror("send");
			  }
			break;
		      }
		    sent = send(new_connection, buffer, BUFFER_LENGTH, 0);
		    if(sent == -1)
		      {
			perror("send");
		      }
		  }
	  

		fclose(file_get);
		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

void remove(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}

	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		/* get filename to be deleted from SDFS */
		char path[PATH_LENGTH];
		memset(&path, 0, sizeof path);
		ssize_t num_recv;
		if((num_recv = recv(new_connection, path, PATH_LENGTH, 0)) == -1)
		{
			perror("recv");
			exit(2);
		}
		path[num_recv] = '\0';

		/* delete file and send response */
		//if(access(("./data/"+ string(path)).c_str(), F_OK) == -1)
		//{
		//	if(send(new_connection, "HTTP/1.0 404 Not Found", strlen("HTTP/1.0 404 Not Found"), 0) == -1)
		//	{
		//		perror("send");
		//		exit(1);
		//	}
		//}
		//else
		//{
			if(remove(("./sdfs_file/"+ string(path)).c_str()) != 0)
			{
				perror("remove");
				exit(1);
			}

		//	if(send(new_connection, "HTTP/1.0 200 OK", strlen("HTTP/1.0 200 OK"), 0) == -1)
		//	{
		//		perror("send");
		//		exit(1);
		//	}
		//}

		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

void replace_file_list(int & socket_file_descriptor, bool & filelist_initialized)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		/* setup write-lock */
		struct flock file_lock;
		file_lock.l_type = F_WRLCK;
		file_lock.l_whence = SEEK_SET;
		file_lock.l_start = 0;
		file_lock.l_len = 0;
		file_lock.l_pid = getpid();

		/* acquire lock and open file */
		int new_file_list;
		if((new_file_list = open("../logger/file_list.txt", O_WRONLY | O_TRUNC)) == -1)
		{
			perror("open");
			exit(1);
		}
		if(fcntl(new_file_list, F_SETLKW, &file_lock) == -1)
		{
			perror("fcntl");
			exit(1);
		}

		/* write new file list to disk */
		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			write(new_file_list, buffer, num_recv);
			memset(&buffer, 0, sizeof buffer);
		}

		/* close file and release lock */
		file_lock.l_type = F_UNLCK;
		if(fcntl(new_file_list, F_SETLK, &file_lock) == -1)
		{
			perror("fcntl");
			exit(1);
		}
		if(close(new_file_list) == -1)
		{
			perror("close");
			exit(1);
		}

		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}
		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}

	if(!filelist_initialized)
	{
		filelist_initialized = 1;
		cout << "OK - SDFS service started" << endl;
	}
}

void onboard(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}

	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		string new_member;
		if((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) == -1)
		{
			perror("recv");
			exit(1);
		}
		else
		{
			buffer[num_recv] = '\0';
			new_member = string(buffer);
		}

		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}

		/* update and broadcast file list */
		map< string, vector<string> > file_list;
		get_file_list(file_list);
		vector<string> no_file;
		file_list[new_member] = no_file;
		update_file_list(file_list);
		broadcast_file_list(file_list);
		update_membrship_list(file_list);

		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

void request_file_list(bool & filelist_initialized)
{
	/* get membership list */
	vector<string> alive_IP;
	while(alive_IP.size() == 0)
	{
		get_alive_IP(alive_IP);
	}

	/* use membership list to initialize or download file list from other VM */
	if(alive_IP.size() == 1) // I am the first VM joining cluster
	{
		map< string, vector<string> > file_list;
		vector<string> no_file;
		file_list[ alive_IP[0] ] = no_file;
		update_file_list(file_list);
		filelist_initialized = 1;
		cout << "OK - SDFS service started" << endl;
	}
	else // downlaod file list: contact other VM about my join and let the VM update and broadcase its new file_list
	{
		string selfIP = get_selfIP(); // get local IP address
		for(size_t i = 0; i < alive_IP.size(); i = i+1)
		{
			if(selfIP.compare(alive_IP[i]) == 0)
			{
				continue;
			}
			else
			{
				int socket_file_descriptor;
				make_socket(socket_file_descriptor, alive_IP[i], JOIN_PORT); // make socket
				if(send(socket_file_descriptor, selfIP.c_str(), selfIP.size(), 0) == -1) // send request
				{
					perror("send");
					exit(1);
				}
				if(close(socket_file_descriptor) == -1)
				{
					perror("close");
					exit(1);
				}
				break;
			}
		}
	}
}

void get_alive_IP(vector<string> & alive_IP)
{
	/* set up read lock */
	struct flock file_lock;
	file_lock.l_type = F_RDLCK;
	file_lock.l_whence = SEEK_SET;
	file_lock.l_start = 0;
	file_lock.l_len = 0;
	file_lock.l_pid = getpid();

	/* open input file with lock */
	int input;
	if((input = open("../logger/member.txt", O_RDONLY)) == -1)
	{
		perror("open");
		exit(1);
	}
	if(fcntl(input, F_SETLKW, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}

	/* put file content into a C++ string called "raw"(easier to manipulate) */
	string raw = "";
	int buffer_size = 18;
	char buffer[buffer_size];
	memset(&buffer, 0, sizeof buffer);
	ssize_t num_read;
	while(1)
	{
		num_read = read(input, buffer, buffer_size);
		if(num_read == -1)
		{
			perror("read");
			exit(1);
		}
		if(num_read > 0)
		{
			buffer[num_read] = '\0';
			raw = raw + string(buffer);
			memset(&buffer, 0, sizeof buffer);
		}
		else
		{
			break;
		}
	}

	/*** unlock and close input file ***/
	file_lock.l_type = F_UNLCK;
	if(fcntl(input, F_SETLK, &file_lock) == -1)
	{
		perror("fcntl");
		exit(1);
	}
	if(close(input) == -1)
	{
		perror("close");
		exit(1);
	}

	/* fill the vector with the C++ string */
	istringstream raw_stream(raw);
	string IP;
	while(getline(raw_stream, IP, '\n'))
	{
		alive_IP.push_back(IP);
	}
}

void update_membrship_list(map< string, vector<string> > & file_list)
{
	/* setup write-lock */
	struct flock fl;
	int fd;
	fl.l_type = F_WRLCK;
	fl.l_whence = SEEK_SET;
	fl.l_start = 0;
	fl.l_len = 0;
	fl.l_pid = getpid();

	/* open file with lock and write */
	fd = open("../logger/member.txt", O_WRONLY | O_TRUNC);
	fcntl(fd, F_SETLKW, &fl); // wait and grap write lock

	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		write(fd, (iter->first).c_str(), (iter->first).size());
		/*
		char temp[(iter->first).size()+1];
		memset(&temp, 0, sizeof(temp));

		int j;
		int length = (int)( (iter->first).size() );
		for(j = 0; j < length; j = j+1)
		{
			temp[j] = (char)( (iter->first)[j] );
		}
		temp[j] = '\n';

		write( fd, temp, sizeof(temp) );
		*/
	}

	fl.l_type = F_UNLCK;
	fcntl(fd, F_SETLK, &fl); // release lock
	close(fd);
}

void check_failure()
{
	/* get file list and member list */
	map< string, vector<string> > file_list;
	get_file_list(file_list);
	vector<string> alive_IP;
	get_alive_IP(alive_IP);

	if(alive_IP.size() < file_list.size()) // failures detected
	{
		string highest_IP = get_highest_IP(alive_IP);
		string selfIP = get_selfIP();
		if(highest_IP.compare(selfIP) == 0) // I am the first replicator, do replication
		{
			sleep(6); // sleep for a while to let other VM finish updating their file_lists

			/* get a list of files that were hosted on failed VM's and remove failed VM's from in-memory file_list*/
			set<string> lost_file;
			get_lost_file(lost_file, alive_IP, file_list);

			/* update and boradcast local file_list */
			update_file_list(file_list);
			broadcast_file_list(file_list);

			replicate(lost_file, file_list, selfIP);
		}
		else
		{
			/* update local file list by removing failed VM's */
			set<string> lost_file;
			get_lost_file(lost_file, alive_IP, file_list);
			update_file_list(file_list);
		}
	}
	//map< string, vector<string> > fl;
	//get_file_list(fl);
	//print_file_list(fl, "at end of check_failure()");
}

string get_highest_IP(vector<string> & alive_IP)
{
	string highest_IP = alive_IP[0];
	for(size_t i = 1; i < alive_IP.size(); i = i+1)
	{
		if(alive_IP[i] > highest_IP)
		{
			highest_IP = alive_IP[i];
		}
	}
	return highest_IP;
}

void get_lost_file(set<string> & lost_file, vector<string> & alive_IP, map< string, vector<string> > & file_list)
{
	vector<string> dead_IP;

	for(map< string, vector<string> >::iterator iter = file_list.begin(); iter != file_list.end(); iter++)
	{
		if(find(alive_IP.begin(), alive_IP.end(), iter->first) == alive_IP.end()) // "alive_IP" does not contain this IP from file list
		{
			for(size_t i = 0; i < iter->second.size(); i = i+1)
			{
				lost_file.insert(iter->second[i]);
			}

			dead_IP.push_back(iter->first);
		}
	}

	for(size_t i = 0; i < dead_IP.size(); i = i+1)
	{
		file_list.erase(dead_IP[i]);
	}
}

void replicate(set<string> & lost_file, map< string, vector<string> > & file_list, string selfIP)
{
	/* do replication, those lost files that are hosted on myself will be removed from "lost_file" */
	replicate(lost_file, file_list[selfIP]);
	/*
	cout << "replicate(2) done" << endl;
	cout << "lost file content:";
	for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
	{
		cout << 	" " << *iter;
	}
	cout << endl;
	cout << "===============================" << endl;
	*/
	/* send "lost_file" to the VM with the next highest IP which will continue replicate the remaining lost files */
	if(!lost_file.empty()) // some lost file are still not replicated because I don't have copy of them on my site
	{
		string next_replicator = get_next_replicator(lost_file); // select a VM with most lost file copies as the next replicator
		int socket_file_descriptor;
		make_socket(socket_file_descriptor, next_replicator, REPLICATE_PORT); // construct socket
		for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
		{
			string message = *iter + "\n";
			if(send(socket_file_descriptor, message.c_str(), message.size(), 0) == -1)
			{
				perror("send");
				exit(1);
			}
		}
		if(close(socket_file_descriptor) == -1)
		{
			perror("close");
			exit(1);
		}
	}
}

void replicate(set<string> & lost_file, vector<string> & local_file)
{
	set<string> replicated_file; // used to update "lost_file" later

	/* do replication */
	for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
	{
		if(find(local_file.begin(), local_file.end(), *iter) != local_file.end()) // this lost file exists in my SDFS
		{
			//cout << "put " << *iter << endl;
			put(iter->c_str(), iter->c_str(), 0);
			replicated_file.insert(*iter);
		}
	}

	/* update "lost_file" */
	for(set<string>::iterator iter = replicated_file.begin(); iter != replicated_file.end(); iter++)
	{
		lost_file.erase(*iter);
	}
}

void replicate_handler(int & socket_file_descriptor)
{
	struct sockaddr_storage their_addr; // connector's address information
	socklen_t sin_size = sizeof their_addr;
	int new_connection = accept(socket_file_descriptor, (struct sockaddr *)&their_addr, &sin_size);
	if(new_connection == -1)
	{
		perror("accept");
		exit(1);
	}
	if(!fork())
	{
		if(close(socket_file_descriptor) == -1) // child doesn't need the listener
		{
			perror("close");
			exit(1);
		}

		char buffer[BUFFER_LENGTH];
		memset(&buffer, 0, sizeof buffer);
		size_t num_recv;
		bool flag = 0;
		string raw = "";
		while((num_recv = recv(new_connection, buffer, BUFFER_LENGTH-1, 0)) > 0)
		{
			buffer[num_recv] = '\0';
			raw = raw + string(buffer);
			memset(&buffer, 0, sizeof buffer);
		}

		if(close(new_connection) == -1)
		{
			perror("close");
			exit(1);
		}
		// cout << "raw is: " << raw << endl;
		/* construct a set of lost files */
		set<string> lost_file;
		istringstream raw_stream(raw);
		string line;
		while(getline(raw_stream, line, '\n'))
		{
			lost_file.insert(line);
		}
		/*
		cout << "ready lost file:" << endl;
		for(set<string>::iterator iter = lost_file.begin(); iter != lost_file.end(); iter++)
		{
			cout << " " << *iter << endl;
		}
		cout << "==========================" << endl;
		*/
		map< string, vector<string> > file_list;
		get_file_list(file_list);

		replicate(lost_file, file_list, get_selfIP());

		exit(0);
	}
	if(close(new_connection) == -1)  // parent doesn't need this
	{
		perror("close");
		exit(1);
	}
}

string get_next_replicator(set<string> & lost_file)
{
	map< string, vector<string> > candidate_pool;
	get_file_list(candidate_pool);
	int max = 0;
	string next_replicator;
	for(map< string, vector<string> >::iterator iter = candidate_pool.begin(); iter != candidate_pool.end(); iter++)
	{
		int num_file = 0;
		for(set<string>::iterator s = lost_file.begin(); s != lost_file.end(); s++)
		{
			if(find(iter->second.begin(), iter->second.end(), *s) != iter->second.end())
			{
				num_file = num_file + 1;
			}
		}
		if(num_file > max)
		{
			max = num_file;
			next_replicator = iter->first;
		}
	}

	return next_replicator;
}
