#!/bin/bash

# Create 10 random binary files whose sizes are 1MB, 2MB, ..., 10MB

cd ./local_file
dd if=/dev/urandom of=1.bin bs=1M count=1
dd if=/dev/urandom of=2.bin bs=1M count=2
dd if=/dev/urandom of=3.bin bs=1M count=3
dd if=/dev/urandom of=4.bin bs=1M count=4
dd if=/dev/urandom of=5.bin bs=1M count=5
dd if=/dev/urandom of=6.bin bs=1M count=6
dd if=/dev/urandom of=7.bin bs=1M count=7
dd if=/dev/urandom of=8.bin bs=1M count=8
dd if=/dev/urandom of=9.bin bs=1M count=9
dd if=/dev/urandom of=10.bin bs=1M count=10
cd ../
