#!/bin/bash

### This script starts all services on VM ###

### start logger ###

cd ./logger
if [ -f ./file_list ]; then
	rm file_list.txt
fi
if [ -f ./member.txt ]; then
	rm member.txt
fi
touch file_list.txt
touch member.txt

make >/dev/null
./http_server 3491 & >/dev/null
cd ../
printf "OK - Logger service started\n"

### start membership service ###
cd ./membership
g++ -o membership_client client.cpp
g++ -o membership_server server.cpp
./membership_client &
./membership_server &
printf "OK - membership service started\n"
cd ../
sleep 6

### start sdfs service ###

cd ./sdfs
if [ -d "./sdfs_file" ]; then
	rm -r ./local_file
fi
if [ -d "./sdfs_file" ]; then
	rm -r ./sdfs_file
fi
mkdir ./sdfs_file
mkdir ./local_file
make >/dev/null
./sdfs_server &
cd ../
