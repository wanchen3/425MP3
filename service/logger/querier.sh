#!/bin/bash

### get membership list from other nodes ###
while read IP; do
	./http_client $IP:3491/member.txt
	if [ $? == 0 ]; then
		printf "From $IP: \n"
		cat output
		rm output
	fi
done <./IP.txt
