How to run our code: 

Step 1: Log into VM1(must be VM1 for setup purposes only)

Step 2: Checkout code in VM1

Step 3: run ./replicate.sh in VM1. This replicates code to all other VM's and do some extra and necessary setup logic. You will probably be asked to enter SSH password to VM's if you haven't setup password-less login

Step 4: cd into to ./service

Step 5: 
	run ./start-all.sh. This will start all relevant processes in background. !!! ATTENTION !!! once you run ./start-all.sh you will see the next two lines be printed out: 

	OK - Logger service started

	OK - membership service started

	wait until you see "OK - SDFS service started" printed on screen before you go to next VM to start-all. This will take few seconds

Step6: 
	Repeat steps 4 and 5 in VM2 - VM7

Step 7: 
	Now you can run PUT, GET, DELETE in any VM by cd into ./service/sdfs.

	To run PUT: 

	./sdfs put local_filename sdfs_filename (TIP: you can run ./generate_test_file.sh under ./service/sdfs to generate test files which will be stored in ./service/sdfs/local_file) Example: ./sdfs put 1.bin 1.bin

	To run GET:

	./sdfs get sdfs_filename local_filename (GETed file is stored under ./service/sdfs/local_file)

	To run DELETE:

	./sdfs delete sdfs_filename

Once you are done, go to ./service and run ./stop-all.sh